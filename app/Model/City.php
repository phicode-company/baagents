<?php
Class City extends AppModel{
	public $name = 'City';
	public $validate = array(
	'name' =>
			array(
				array(
					'rule' => 'notEmpty',
					'message' => 'Please enter city name.'
				) 
			),
	'country_id' =>
			array(
				array(
					'rule' => 'notEmpty',
					'message' => 'Please select country.'
				) 
			),
	'TimeZone' =>
			array(
				array(
					'rule' => 'notEmpty',
					'message' => 'Please select time zone.'
				) 
			),
	'slug'=>  array( 
			'notEmpty'=>array(
				'rule' =>array('notEmpty'),
				'message'=> 'Please enter slug url.'
				),
			'isUnique'=>array(
				'rule'=>array('isUnique'),
				'message'=>'This url is already associated with other city.'
				)
			),
	'payout_options'=>  array( 
			'notEmpty'=>array(
				'rule' =>array('multiple'),
				'message'=> 'Please select at least one payout option.'
				)
			),
	'default_payout_options'=>  array(
			'notEmpty'=>array(
				'rule' =>array('notEmpty'),
				'message'=> 'Please select a default payout option.'
				),
			'isValid'=>array(
				'rule' =>array('isValid'),
				'message'=> 'Invalid default payout option.'
				)
			),
	);
	
	function isValid(){		
		if(!empty($this->data['City']['payout_options']) && !in_array($this->data['City']['default_payout_options'],$this->data['City']['payout_options']) ){
			return false;
		}
		return true;
	}
	function getCityPopularList($options = array()){
		$conditions = array();
		$joins = array();
		if(!empty($options['country_id'])){
			$conditions['City.country_id'] = $options['country_id'];	
		}
		if(!empty($options['id'])){
			$conditions['City.id'] = $options['id'];	
		}
		
		if(!empty($options['agent_id'])){
			$joins = array(
									array('table' => 'agent_cities',
											'alias' => 'AgentCity',
											'type' => 'INNER',
											'conditions' => array(
													'AgentCity.agent_id = '.$options['agent_id'],
													'AgentCity.city_id = City.id'
											)
									)
								);
		}
		
		$conditions['City.status'] = 1;
		$conditions['City.popular'] = 1;
		$city = $this->find('list',array('conditions'=>$conditions,'joins'=>$joins,'fields'=>array('City.id','City.name')));
		return $city;
	}

}
?>
