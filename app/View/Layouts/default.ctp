<?php ?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if ($this->params['controller']=='courses' && $this->params['action']=='search'){?>
<meta name="viewport" content="width=device-width, user-scalable=no, target-densitydpi=160dpi, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />
<?php } else { ?>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />
<?php } ?>
<title><?=$title_for_layout;?></title>
<?php echo $this->Html->meta('favicon.ico','/favicon.ico',array('type' => 'icon') ); ?>

<meta name="keywords" content="<? //=$metakeyword;?>" />
<meta name="description" content="<? //=$metadescription;?>" />


<!--og:image:secure_url-->



<!-- BEGIN Load JS -->
<?php echo $this->Html->script('jquery-1.11.0.min.js');?>



<!-- BEGIN Load CSS -->
<?php
	echo $this->Html->css('combined.min.css?v2.07.55.14');
	echo $this->Html->css('style.css?v1.05.01');
//	echo $this->Html->css('main-css.css');	
//	echo $this->Html->css(Router::url('/').'plugins/darktooltip-master/css/darktooltip.css');
?>
<?php echo $this->Html->css(array('flexslider.css?version=1.1.6','jquery.fancybox.css')); ?>



<!-- END Load CSS -->
<script type="text/javascript" src="/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="/js/jquery-ui.js"></script>
<script type="text/javascript" src="/plugins/darktooltip-master/js/jquery.darktooltip.js"></script>
<script type="text/javascript" src="/js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="/js/retina.min.js" ></script>
<script type="text/javascript" src="/js/chosen.jquery.js"></script>
</head>



<?php $class='';
if(in_array($this->params['controller'],array('pages')) && in_array($this->params['action'],array('home','index'))){
	$class = 'site-home';
}?>
<?php if(in_array($this->params['controller'],array('presses','invites'))){
	$class = 'press-home';
}?>

<body class="<?=$class?>">


<div class="container-for-mobile">
<!-- Starts header -->
<div class="normal-header<?php //echo ((!empty($page)) && ($page['Page']['id']==3))?'fixed-header':'normal-header'; ?>">
<div class="main-header-div">
	<?php $messageStyle = empty($userId) ? 'display:none;' : '';?>
	<?php if($this->Session->check('Message.newPayoutFeature')){ ?>
	<div class="notification-top" style="<?=$messageStyle?>">
	   <div class="wrapper">
		   <div class="label-top"><?=$this->Session->flash('newPayoutFeature',array('element'=>false)); ?></div>
	   </div>
	</div>
	<?php } ?>
	<?php if($this->Session->check('Message.CourseCancellation')){ ?>
	<div class="notification-top" style="<?=$messageStyle?>">  
	   <div class="wrapper">v2.04.82
		   <div class="label-top"><?=$this->Session->flash('CourseCancellation',array('element'=>false)); ?></div>
	   </div>
	</div>
	<?php } ?>
	
	<?php if(!empty($userId) && ($is_email_verified==0)){ ?>
		<?php //echo $this->element('CourseManager.users/user_resend_mail',array('user_name'=>$front_user_name,'email'=>$user_email)); ?>
	<?php } ?>
	
	<div class="header-bg">
		<div class="header-wrapper">
			<?php echo $this->element('header');?>
		</div>
		<div id="pre_loader_front" style="left: 50%; position: absolute; display:none; top: 200px;">
		<?=$this->Html->image('fancybox_loading.gif',array('alt'=>'fancybox_loading'));?></div>
	</div>
</div>
</div>

	<?php if ((in_array($this->params['controller'],array('courses','traveler_tales','pages','presses','invites')) && in_array($this->params['action'],array('home','search','index','listing','invite','sign_up'))) || ($this->params['controller']=="pages" && $this->params['action']=="view" && $this->params['pass'][0]==3)){?>
		<?php echo $this->fetch('content'); ?>
	<?php } else {  ?>
		<div class="<?php //echo $content_class;?>inner-content">
			<div class="wrapper">
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
	<?php } ?>

<!-- Starts Footer -->
	<div class="footer-bg">
		<div class="wrapper">
			<?php echo $this->element('footer');?>
		</div>
	</div>
<!-- Ends Footer -->
</div>



<?php echo $this->Html->script('nav-two.js?v=1.0');?>
<?php echo $this->Html->script('classie.js?v=1.0');?>
<?php echo $this->Html->script('jquery.flexslider.js');?>
<?php echo $this->Html->script('app.js?v=1.0.0.7.162');?>

</body>
</html>
