
					<!--Popup for login-->
    <h3 class="heading-mt">Login</h3>
 <div class="white-back-full">
    <div class="fields-page">
		<div class="">
        <div id="login" style="width:625px;">
			<div class="left-popup-content">
			<h1>Visit the Backstreets</h1>
			<p>Immerse yourself in the backstreets and find your self creating a local craft, experiencing authentic tours and meeting the locals that shape 
				the culture of a place.</p>
			<p>Don’t have an account? <a href="/users/signup" class="various">Sign up</a></p>
			</div> 
			<div class="right-popup-content">
				<h4>Join us to explore the Backstreets</h4>
				<div style="/*display:none;*/">

<script>
	jQuery(document).ready(function() {		
		jQuery(".message-notification").find(".close").bind('click',function(){
			jQuery(this).parent().parent().fadeOut(500);
		});
	});
</script>
</div>
				<!--
				<div class="facebook-btn"><a href="javascript:void(0)" onclick="authorizeAppInPopup()"><img src="/img/facebook-login-btn.png" alt="Login with Facebook"></a><div class="or">OR</div></div>
				-->

<script>
	jQuery(document).ready(function() {		
		jQuery(".message-notification").find(".close").bind('click',function(){
			jQuery(this).parent().parent().fadeOut(500);
		});
	});
</script>
				


<form action="/users/login" name="user" id="UserLogin" novalidate="novalidate" class="" method="post" accept-charset="utf-8"><div style="display:none;"><input name="_method" value="POST" type="hidden"></div>				<input name="data[User][form-name]" value="UserLoginForm" id="UserForm-name" type="hidden">				<!--?//='asdf'.$user_email.' - '.$user_password;die;?-->
				<input name="data[User][email]" placeholder="Email Address" value="" id="UserEmail_1" type="email">				<input name="data[User][password]" placeholder="Password" value="" id="UserPassword_1" type="password">				<div class="extra">
					
					<div class="left">
						<input name="data[User][remember]" value="1" id="UserRemember" type="checkbox"><label for="UserRemember">Remember Me</label>
					</div>
					<div class="right">
						<a href="/users/resetpassword" class="various fancybox.ajax">Forgot Password?</a>
				</div>
			</div>
			<div><input value="Login" type="submit"><div id="pre_loader" class="reloader"><img src="/img/preloader.png" alt=""></div></div>
			</form>		</div>
	</div>   
    </div> 
  </div>
  <div class="clear"></div>
 </div>
    
 	<!--end #login-->
	
 <script type="text/javascript">
			var Id = "";
	$(document).ready(function(){
		if(Id){
			$.fancybox.close();
			location.reload();
			//window.location.href = "/course_manager/users/redirect_user";
		}
		$('#UserLogin').submit(function(){
			var Fieldsdata = new FormData(this);
			var formData = $(this);
				var status = 0;
			$.each(this,function(i,v){
				$(v).removeClass('invalid form-error');
			});
			$('.invalid-login').remove();
			$('.error-message').remove();
			$('#UserLogin &gt; span#for_owner_cms').show();
			$('#UserLogin &gt; button[type=submit]').attr({'disabled':true});
			$('.reloader').show();
			$.ajax({
					url: '/course_manager/users/ajax_validation',
					async: false,
					data: Fieldsdata,
					dataType:'json', 
					type:'post',
					cache: false,
					contentType: false,
					processData: false,
					success: function(data) {						
						if(data.error==1){
							$('.reloader').hide();
							$.each(data.errors,function(i,v){
							$('#'+i+'_1').addClass("invalid form-error").after('&lt;div class="error-message"&gt;'+v+'&lt;/div&gt;');
								$('#'+i+'_1').bind('click',function(){
									$(this).removeClass('invalid form-error');
									$(this).next('.error-message').remove();
								});
							});
						}else{
							status = 1;
						}
					
					}
				});
				if(status==0){
					//$("html, body").animate({ scrollTop: 0 }, "slow");
					$('#UserLogin &gt; button[type=submit]').attr({'disabled':false});
					$('#UserLogin &gt; span#for_owner_cms').hide();
					return false; 
				}
			return (status===1)?true:false; 
			/* Not in use to redirect user
			var Status = 1;
			$.ajax({
					url: '/course_manager/users/login',
					async: false,
					data: Fieldsdata,
					dataType:'json', 
					type:'post',
					cache: false,
					contentType: false,
					processData: false,
					success: function(Data) {
						if(Data.error==1){
							$('.reloader').hide();
							$('div.facebook-btn').after('&lt;div class="invalid-login" style="color:#FF0000;"&gt;'+Data.errors+'&lt;/div&gt;');
						}else{
							Status = 0;
						}
					}			
					
				});
				if(Status==0){
					$.fancybox.close();
					location.reload();
					//window.location.href = "/course_manager/users/redirect_user";
					return false;
				}else{
					return false;
				}*/
			});
		});
 </script> 

<script>
var click_on = '';//To open popup of that page, where this page is seen

  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
		
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      ////document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      ////document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  
  // Run auto on page load to check fb login or not
  function checkLoginState() {	
    FB.getLoginStatus(function(response) {
      //statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {	
  FB.init({
    appId      : 790771184408980,
    status     : true, // check login status
    cookie     : true,  // enable cookies to allow the server to access // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.8' // use version 2.0
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  // Run auto on page load to check fb login or not
  FB.getLoginStatus(function(response) {
    //statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI(apiresponse) {
	//var Status = 1;
    //console.log('Welcome!  Fetching your information.... ');
    //console.log(apiresponse);
    //return;
    FB.api('/me?fields=email,first_name,last_name,gender,birthday,hometown,about,location,likes,photos', function(response) {		
		var auth_response = FB.getAuthResponse();
		var location;
		//console.log(response);
		//return;
		if(response.location!==undefined &amp;&amp; response.location!=='undefined'){
			$.ajax({
			type: 'GET',
			url: 'https://graph.facebook.com/'+response.location.id+'?fields=location', 
			async: false,
			data: {
				access_token: auth_response.accessToken//received via response.authResponse.accessToken after login
				
			},
			success: function(response) {
				location = response;
				console.log(response);
			}
			});
			response.city_detail = location;
		}
		
		//console.log(response);
		//return;
	
		/*$.each(response,function(i,v){
			alert(i+' =&gt; '+v);
		});*///return false;
		//response.login_from_fb = 1;
		$.ajax({
			url: '/course_manager/users/fb_login',
			async: false,
			data: response,
			dataType:'json', 
			type:'post',
			success: function(Data) {
				if(Data.error==0) {
					if(Data.new_user == 1){
						fbq('track', "CompleteRegistration"); //Facebook Pixel Code
					}
					
											window.location.reload();
									}else{
					$('#pre_loader_front').hide();
					$('.facebook-btn').prev('div').html(Data.error_msg);
					//return false;
				}
			}
		});
		
      //console.log('Successful login for: ' + response.name);
      ////document.getElementById('status').innerHTML ='Thanks for logging in, ' + response.name + '!';
    });
  }
  function authorizeAppInPopup(){
	FB.login(function(response) {
	  if (response.status === 'connected') {
		  //console.log(response);
			//$.fancybox.close();
			$('#pre_loader_front').show();
		// Logged into your app and Facebook.
		testAPI(response);
	  } else if (response.status === 'not_authorized') {
		// The person is logged into Facebook, but not your app.
		////document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
	  } else {
		// The person is not logged into Facebook, so we're not sure if
		// they are logged into this app or not.
		////document.getElementById('status').innerHTML = 'Please log ' +'into Facebook.';
	  }
	}, {scope: 'public_profile, email, user_location, user_hometown, user_likes, user_birthday'}); /*This scope is currently not working*/
  }
</script>
